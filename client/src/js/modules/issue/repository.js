angular.module('laboard-frontend')
    .factory('IssuesRepository', [
        '$rootScope', 'Restangular', '$q', 'IssuesSocket',
        function($root, $rest, $q, $socket) {
            var fetch = function(self) {
                    return $rest.all('projects/' + $root.project.path_with_namespace + '/issues')
                        .getList()
                        .then(function (issues) {
                            issues.map(function(issue) {
                                issue.tags = [];

                                issue.labels.forEach(function(label, key) {
                                    issue.tags[key] = _.find($root.project.labels, { name: label });
                                });

                                return issue;
                            });

                            self.$objects = _.sortBy(issues, 'iid');

                            return self.$objects;
                        });
                },
                fetchOne = function (id, self) {
                    return $q.all([
                        $rest.all('projects/' + $root.project.path_with_namespace).one('issues', id).get(),
                        $root.project.labels
                    ]).then(function(values) {
                        var issue = values[0],
                            labels = values[1];

                        issue.tags = [];

                        issue.labels.forEach(function(label, key) {
                            issue.tags[key] = _.find(labels, { name: label });
                        });

                        return self.add(issue);
                    });
                },
                all = function (self) {
                    if (!$root.project) {
                        throw new Error;
                    }

                    return fetch(self)
                        .then(function(issues) {
                            if (issues.length) {
                                self.all = function() {
                                    return allCached(self);
                                };
                            }

                            return issues;
                        });
                },
                allCached = function(self) {
                    return $q.when(self.$objects);
                },
                repository = {
                    $objects: null,

                    all: function() {
                        return all(this);
                    },
                    one: function(id) {
                        return fetchOne(id, this);
                    },

                    add: function(issue) {
                        var added = false;

                        this.$objects = this.$objects || [];

                        this.$objects.forEach(function(value, key) {
                            if (added) { return; }

                            if (value.id === issue.id) {
                                this.$objects[key] = issue;
                                added = true;
                            }
                        }, this);

                        if (added === false && issue.id) {
                            this.$objects.push(issue);
                        }

                        return issue;
                    },
                    unadd: function (issue) {
                        this.$objects.forEach(function(value, key) {
                            if (value.id === issue.id) {
                                this.$objects.splice(key, 1);
                            }
                        }, this);

                        return issue;
                    },
                    clear: function() {
                        this.$objects = null;

                        return this.uncache();
                    },
                    uncache: function() {
                        this.all = function() {
                            return all(this);
                        };

                        return this;
                    },

                    persist: function (issue) {
                        if (issue.assignee) {
                            issue.assignee_id = issue.assignee.id;
                        }

                        return $rest.all('projects/' + $root.project.path_with_namespace)
                            .one('issues', issue.iid)
                            .customPUT(issue)
                            .then(this.add.bind(this));
                    },
                    move: function(issue) {
                        return $rest.all('projects/' + $root.project.path_with_namespace)
                            .one('issues', issue.iid)
                            .customPUT(issue, 'move')
                            .then(this.add.bind(this));
                    },
                    theme: function(issue) {
                        return $rest.all('projects/' + $root.project.path_with_namespace)
                            .one('issues', issue.iid)
                            .customPUT(issue, 'theme')
                            .then(this.add.bind(this));
                    },
                    star: function(issue) {
                        var self = this;

                        return $rest.all('projects/' + $root.project.path_with_namespace)
                            .one('issues', issue.iid)
                            .customPUT(issue, 'star')
                            .then(function(issue) {
                                if (issue.state === 'closed') {
                                    self.unadd(issue);
                                } else {
                                    self.add(issue);
                                }

                                return issue;
                            });
                    },
                    close: function(issue) {
                        var self = this;

                        return $rest.all('projects/' + $root.project.path_with_namespace)
                            .one('issues', issue.iid)
                            .customPUT(issue, 'close')
                            .then(function(issue) {
                                if (!issue.starred) {
                                    self.unadd(issue)
                                }

                                return issue;
                            });
                    }
                };

            var handler = function(data) {
                repository.one(data.issue.id).then(function(issue) {
                    if (issue.state === 'closed' && issue.starred === false) {
                        repository.unadd(issue);
                    }
                });
            };

            $socket
                .on('update', handler)
                .on('move', handler)
                .on('theme', handler)
                .on('star', handler)
                .on('edit', handler)
                .on('close', handler);

            return repository;
        }
    ]);
